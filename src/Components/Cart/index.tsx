import { IProduct, useCart } from "../../Providers/Cart"
import ProductCard from "../ProductCard";


const Cart = () => {

    const { cart } = useCart();
    console.log(cart);

    return(
        <>
        {cart?.map((el: IProduct, index) => <ProductCard isInCart={true} currentProduct={el} key={index}/> )}
        </>
    )
}

export default Cart;