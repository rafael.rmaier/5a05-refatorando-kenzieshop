import { Card } from './style';
import { IProduct, useCart } from '../../Providers/Cart';
import Button from '../Button';
import { formatter } from '../../Services/formatter';

interface ProductCardProps {
    currentProduct: IProduct;
    isInCart: boolean;
}


const ProductCard = ({currentProduct, isInCart} : ProductCardProps) => {

    const { addToCart, removeFromCart } = useCart();

    return(
        <Card isInCart={isInCart}>
        <img src={currentProduct.image_url} alt={currentProduct.name}/>
        <div className="textDiv">

            <h2>{currentProduct.name}</h2>
            <h3>{currentProduct.description}</h3>
            <h2>{formatter.format(currentProduct.price)}</h2>
        </div>
            <div className="buttonDiv">

                {isInCart? <Button classNameProp="removeFromCartBtn" onClick={() => removeFromCart(currentProduct)}>Remover</Button>
                :
                <Button classNameProp="addToCartBtn" onClick={() => addToCart(currentProduct)}>Adicionar ao carrinho</Button>}
            </div>
        </Card>
    )

}

export default ProductCard;
