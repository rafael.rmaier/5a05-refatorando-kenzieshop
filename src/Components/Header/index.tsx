import { useState, useEffect } from 'react';
import {RiShoppingCartLine} from 'react-icons/ri';
import { useHistory } from 'react-router';
import { useCart } from '../../Providers/Cart';
import { useUser } from '../../Providers/User';
import Button from '../Button';
import { Container } from './styles';
import {FiLogIn} from 'react-icons/fi';
import { FiLogOut } from 'react-icons/fi';
import { CgLogOut } from 'react-icons/cg';

const Header = () => {

    const {  isLoggedIn, logout } = useUser();


    const { cart } = useCart();

    const history = useHistory();

    const [cartSize, setCartSize] = useState(cart.length);

    const handleClickToCart = () => {
        history.push("/cart");

    }

    const handleClickToLogin = () => {
        history.push("/login");
    }

    const handleClickToHome = () => {
        history.push("/");
    }
    useEffect(() => {
        setCartSize(cart.length);
       
      }, [cart.length, isLoggedIn]);
    

    return(
        <Container>
            <Button classNameProp="navigationBtn" onClick={handleClickToHome}>HOME</Button>
            <div className="cartDiv">
                <Button classNameProp="shoppingCartSpan" onClick={handleClickToCart}>
                <span className="cartAndCounter">
                    <RiShoppingCartLine/>
                    <span className="cartSize">{cartSize}</span>

                </span>
                <span>CARRINHO</span>
                </Button>
                <span className="loginLogoutSpan">

                    {isLoggedIn?
                    <Button onClick={logout} classNameProp="navigationBtn"><CgLogOut/> LOGOUT</Button>
                    :
                    <Button onClick={handleClickToLogin} classNameProp="navigationBtn"><FiLogIn/> LOGIN</Button>

                    }
                </span>

            </div>

        </Container>
    )
}

export default Header;