import React from 'react';
import Header from './Components/Header';



import Routes from './Routes';
import { GlobalStyle } from './Styles/global';

function App() {
  return (
    <div className="App">
        <GlobalStyle />
        <Header/>
        <Routes/>

    </div>
  );
}

export default App;
