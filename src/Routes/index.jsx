import { Switch, Route} from 'react-router-dom';
import CartPage from '../Pages/CartPage';
import Login from '../Pages/Login';
import Showcase from '../Pages/Showcase';


const Routes = () => {

    return(
        <Switch>
            <Route component={CartPage} path="/cart"/>
            <Route component={Showcase} exact path="/" />
            <Route component={Login} exact path="/login" />
        </Switch>
        
    )
}

export default Routes;