import styled, {css} from 'styled-components';



export const StyledButton = styled.button.attrs((props) => ({
    className: props.className,
}))`

position: relative;
cursor: pointer;
border: transparent;
border-radius: 0.2rem;
transition: 0.3s;

&.purchaseBtn{
    background-color: var(--purchaseButtons);
    opacity: 0.8;
    color: white;
    transition: 0.3s ease-in;
    @media (min-width: 421px) and (max-width: 768px){
        height: 20%;
        width: 100%;
        font-size: 1.2rem;
     }
     @media (min-width: 769px){
        height: 20%;
        width: 100%;
        font-size: 1.1rem;
     }

}

&.purchaseBtn:hover{
    background-color: #124BFA;

    transition: 0.3s ease-in;
}

&.addToCartBtn {
    background-color: var(--purchaseButtons);
    color: #fff;
    opacity: 0.9;
    width: 100%;
    @media(max-width: 420px){
        
        height: 100%;
        font-size: 1.2rem;
    }
    @media (min-width: 421px) and (max-width: 768px){
       height: 50%;
       width: 100%;
       font-size: 1.2rem;
    }
    @media (min-width: 769px){
        height: 80%;
    }
}

&.addToCartBtn:hover{
    background-color: #124BFA;

    transition: 0.3s ease-in;
}

&.removeFromCartBtn{
    background-color: #FA3744;
    color: white;
}

&.removeFromCartBtn:hover{
    background-color: palevioletred;

}

&.switchRegisterLogin{
    background-color: transparent;
}

&.switchRegisterLogin:hover{
    background-color: #ededed;
}

&.submitBtn{
    background-color: var(--kenzieblue);
    color: white;
    margin: 15px;
}



`