import axios from "axios";
import { useEffect, useState } from "react";
import { IProduct, useCart } from "../../Providers/Cart";
import ProductCard from "../ProductCard";
import { ProdContainer } from './styles';
import loadingsvg from '../../Assets/Images/Spinner-1s-200px.svg';



const ProductsList = () => {

    const [isLoading, setIsLoading] = useState(true);

    const [productsList, setProductsList] = useState<IProduct[]>([] as IProduct[]);

    useEffect(() => {

        axios
        .get("https://kenzieshop2.herokuapp.com/products")
        .then((res) => {
            setProductsList(res.data);
            setIsLoading(false);
        })
        .catch(err => console.log(err));
    }, [])


    return(
        <ProdContainer>
            {isLoading? <img src={loadingsvg} alt="loading"/>
            :
            productsList?.map((el: IProduct, index) => <ProductCard currentProduct={el} isInCart={false} key={index}/>)}
     
        </ProdContainer>
    )

}

export default ProductsList;