import styled from 'styled-components';

export const Container = styled.div`
figure{
    display: none;
}
width: 100vw;
height: 100vh;
display: flex;
flex-flow: row wrap;
align-items: center;
justify-content: center;
align-content: center;
@media (min-width: 481px){
    flex-flow: column wrap;
}
@media (min-width: 769px) and (max-width: 1024px){
    figure{
        max-width: 50%;
        max-height:50%;
    }
    img{
        max-width: 100%;
    }
}
@media (min-width: 769px){
   
    figure{
        display: block;
        width: 40%;
        height: 40%;
    }
    div.formsContainer{
        width: 40%;
        text-align: center;
    }
    flex-flow: row wrap;
    column-count: 2;
}
h6{
    text-align: center;
}


`