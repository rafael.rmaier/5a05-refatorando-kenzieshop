import { useForm } from "react-hook-form";
import * as yup from 'yup';
import { yupResolver } from "@hookform/resolvers/yup";
import Button from "../../Button";
import { IUserData, useUser } from '../../../Providers/User';
import { FormLogin } from "./style";

const LoginForm = () => {

    const { login } = useUser();

    const schema = yup.object().shape({
     
        email: yup.string().email("Formato inválido").required("Campo obrigatório"),
        password: yup.string().required("Campo obrigatório"),
    })

    const { register, handleSubmit, formState: { errors }} = useForm<IUserData>({
        resolver: yupResolver(schema)
    });
    
    const onSubmit = (data: IUserData) => {
        login(data);
        console.log(data);
    }

    return(
        <FormLogin onSubmit={handleSubmit(onSubmit)}>
            
            <input type="text" {...register("email")} placeholder="Email" />

            <div className="errorDiv">
                <p className="errorMsg">{errors.email && errors.email.message}</p>
            </div>

            <input type="password" {...register("password")} placeholder="Senha"/>

            <div className="errorDiv">
                {errors.password && <p className="errorMsg">{errors.password.message}</p>}
            </div>

            <Button type="submit" classNameProp="submitBtn">Login</Button> 
        </FormLogin>
    )

}

export default LoginForm;