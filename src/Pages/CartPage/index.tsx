import Cart from "../../Components/Cart";
import CartReducer from "../../Components/CartReducer";
import { Container } from "./style";



const CartPage = () => {

    return(
        <Container>
            <div className="orderSummaryDiv">
                <CartReducer/>

            </div>
            <div className="cartDiv">
                <Cart />

            </div>

        </Container>
    )

}

export default CartPage;