import { useState } from "react";
import { Container } from './styles';
import LoginForm from "../../Components/Forms/LoginForm";
import RegisterForm from "../../Components/Forms/RegisterForm";
import Button from "../../Components/Button";
import login from '../../Assets/Images/login.svg';


const Login = () => {

    const [isRegistered, setIsRegistered] = useState(true);

    const handleIsRegister = () => {
        setIsRegistered(!isRegistered);
    }

    return(
        <Container>
            <figure>
                <img src={login} alt="login"/>
            </figure>
            {isRegistered?
            <div className="formsContainer">
                <LoginForm/>
                <h6>Não possui uma conta?<Button onClick={handleIsRegister} classNameProp="switchRegisterLogin">Cadastre-se</Button></h6>
            </div>
            :
            <div className="formsContainer">
                <RegisterForm/>
                <h6>Já possui uma conta?<Button onClick={handleIsRegister} classNameProp="switchRegisterLogin">Entrar</Button></h6>
            </div>
            }
        </Container>
    )
}

export default Login;
